package org.apache.cordova.plugin;

import android.app.Fragment;
import android.content.Context;
import android.content.BroadcastReceiver;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.BatteryManager;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Admin on 27/03/2017.
 */

public class PowerConnectionReceiver extends BroadcastReceiver {
    static Echo echo;

    public void setCallback(Echo echo){
        this.echo = echo;
    }
    @Override
    public void onReceive(Context context, Intent intent) {
        if(PowerConnectionReceiver.echo != null){
            String action = intent.getAction();
            JSONObject jsonResult = new JSONObject();

            if(action.equals(Intent.ACTION_POWER_CONNECTED)){
                try {
                    jsonResult.accumulate("batteryPercent", true);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else if(action.equals(Intent.ACTION_POWER_DISCONNECTED)){
                try {
                    jsonResult.accumulate("batteryPercent", false);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            PowerConnectionReceiver.echo.callback(jsonResult);
        }
    }
}