package org.apache.cordova.plugin;

import org.apache.cordova.CordovaPlugin;
import org.apache.cordova.CallbackContext;

import org.apache.cordova.PluginResult;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Intent;
import android.content.IntentFilter;
import android.os.BatteryManager;

public class Echo extends CordovaPlugin {
    private PowerConnectionReceiver powerConnectionReceiver;
    private CallbackContext callbackContext;

    @Override
    public void onResume(boolean multitasking) {
        super.onResume(multitasking);
    }

    @Override
    public boolean execute(String action, JSONArray args, CallbackContext callbackContext) throws JSONException {
        if (action.equals("setCallback")) {
            this.callbackContext = callbackContext;
            powerConnectionReceiver = new PowerConnectionReceiver();
            powerConnectionReceiver.setCallback(this);
            return true;
        }
        return false;
    }

    public void callback(JSONObject jsonResult){
        PluginResult result = new PluginResult(PluginResult.Status.OK, jsonResult);
        result.setKeepCallback(true);
        callbackContext.sendPluginResult(result);
    }
}